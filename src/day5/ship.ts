import {Stack} from './stack';

export class Ship {
    private stacks: Stack[]

    public constructor() {
        this.stacks = []
    }

    public init(stack: number, val: string) {
        while(this.stacks.length < stack) {
            this.stacks.push(new Stack())
        }

        this.stacks[stack-1].addBottom(val)
    }

    public move(from: number, to: number) {
        const crate = this.stacks[from - 1].pop()
        if (crate) {
            this.stacks[to - 1].add(crate)
        }
    }

    public pick(from: number): string | undefined {
        return this.stacks[from-1].pop()
    }

    public put(to: number, val: string) {
        this.stacks[to-1].add(val)
    }

    public survey(): string {
        const vals: string[] = []

        for (const s of this.stacks) {
            const val = s.peek()
            if (val) {
                vals.push(val)
            }
        }

        return vals.join('')
    }

}
