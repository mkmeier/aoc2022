import {Day5, parseCargoLine} from './puzzle';

describe('parseCargoLine', () => {
    it('should return a map', () => {
        const input = '[C]     [Q] [J]         [V] [Q] [D]'
        const expected = new Map<number, string>()

        expected.set(1, 'C')
        expected.set(3, 'Q')
        expected.set(4, 'J')
        expected.set(7, 'V')
        expected.set(8, 'Q')
        expected.set(9, 'D')

        expect(parseCargoLine(input)).toEqual(expected)
    })
})

describe('part 1', () => {
    it('should parse', () => {
        const input = [
            "[Q]         [N]             [N]    ",
            "[H]     [B] [D]             [S] [M]",
            "[C]     [Q] [J]         [V] [Q] [D]",
            "[T]     [S] [Z] [F]     [J] [J] [W]",
            "[N] [G] [T] [S] [V]     [B] [C] [C]",
            "[S] [B] [R] [W] [D] [J] [Q] [R] [Q]",
            "[V] [D] [W] [G] [P] [W] [N] [T] [S]",
            "[B] [W] [F] [L] [M] [F] [L] [G] [J]",
            " 1   2   3   4   5   6   7   8   9 ",
        ]

        const pzl = new Day5()
        const result = pzl.part1(input)
        const r = result
    })

    it('should move', () => {
        const input = [
            "    [D]    ",
            "[N] [C]    ",
            "[Z] [M] [P]",
            " 1   2   3 ",
            "",
            "move 1 from 2 to 1",
            "move 3 from 1 to 3",
            "move 2 from 2 to 1",
            "move 1 from 1 to 2",
        ]

        const pzl = new Day5()
        const result = pzl.part1(input)
        expect(result).toEqual('CMZ')
    })
})
