
export class Stack {

    private readonly crates: string[]

    public constructor() {
        this.crates = []
    }

    public addBottom(val: string): void {
        this.crates.splice(0, 0, val)
    }

    public add(val: string): void {
        this.crates.push(val)
    }

    public pop(): string | undefined {
        return this.crates.pop()
    }

    public peek(): string | undefined {
        if (this.crates.length === 0) {
            return
        }

        return this.crates[this.crates.length - 1]
    }
}
