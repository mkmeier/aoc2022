import {Puzzle} from '../puzzle';
import {Stack} from './stack';
import {Ship} from './ship';

export class Day5 implements Puzzle {
    part1(input: string[]): string {
        const ship = new Ship()

        let stateLoading = true
        let currentState = ''
        for (const line of input) {
            if (stateLoading && line.startsWith(" 1")) {
                stateLoading = false
            }

            if (stateLoading) {
                const p = parseCargoLine(line)
                for (const c of p.keys()) {
                    const val = p.get(c)
                    if (val) {
                        ship.init(c, val)
                    }

                }
            } else if (line.startsWith('m')) {
                const parts = line.split(' ')
                const count = parseInt(parts[1], 10)
                const from = parseInt(parts[3], 10)
                const to = parseInt(parts[5], 10)

                for (let i = 0; i < count; i++) {
                    ship.move(from, to)
                }
                currentState = ship.survey()
            }
        }

        return ship.survey()
    }

    part2(input: string[]): string {
        const ship = new Ship()

        let stateLoading = true
        let currentState = ''
        for (const line of input) {
            if (stateLoading && line.startsWith(" 1")) {
                stateLoading = false
            }

            if (stateLoading) {
                const p = parseCargoLine(line)
                for (const c of p.keys()) {
                    const val = p.get(c)
                    if (val) {
                        ship.init(c, val)
                    }

                }
            } else if (line.startsWith('m')) {
                const parts = line.split(' ')
                const count = parseInt(parts[1], 10)
                const from = parseInt(parts[3], 10)
                const to = parseInt(parts[5], 10)

                const buffer: string[] = []
                for (let i = 0; i < count; i++) {
                    const v = ship.pick(from)
                    if (v) {
                        buffer.splice(0, 0, v)
                    }
                }

                for (const crate of buffer) {
                    ship.put(to, crate)
                }

                currentState = ship.survey()
            }
        }

        return ship.survey()
    }
}

export function parseCargoLine(l: string): Map<number, string> {
    const m = new Map<number, string>()

    for (let i = 0; i * 4 < l.length; i++) {
        const index = i *4
        const v = l.slice(index, index + 3)
        if (v.trim().length > 0) {
            m.set(i+1, v.slice(1,2))
        }
    }

    return m
}
