import {Stack} from './stack';

describe('stack', () => {
    it('peak should have the last element added', () => {
        const s = new Stack()
        s.add("a")
        s.add("b")

        expect(s.peek()).toEqual("b")
    })

    it('should return the last element on pop', () => {
        const s = new Stack()
        s.add("a")
        s.add("b")

        expect(s.pop()).toEqual("b")
        expect(s.peek()).toEqual("a")
    })

    it('should add elements to the bottom', () => {
        const s = new Stack()
        s.add("a")
        s.add("b")
        s.addBottom("c")

        expect(s.pop()).toEqual("b")
        expect(s.pop()).toEqual("a")
        expect(s.pop()).toEqual("c")
    })
})
