import {File} from "./File";

export class Dir {
    public name: string
    public parent: Dir | undefined
    public files: File[] = []
    public directories: Dir[] = []
    public size = 0

    constructor(name: string, parent: Dir|undefined) {
        this.name = name
        this.parent = parent
    }

    public addDir(name: string) {
        const dir = new Dir(name, this)
        this.directories.push(dir)
    }

    public addFile(name: string, size: number) {
        const file = new File(name, size)
        this.files.push(file)
        this.addSize(size)
    }

    public addSize(size: number) {
        this.size += size
        if (this.parent) {
            this.parent.addSize(size)
        }
    }

    // tslint:disable:no-console
    public print(spacing = "") {

        console.log(`${spacing}- /${this.name}: ${this.size}`)
        this.directories.forEach(d => d.print(spacing + "  "))
        this.files.forEach(f => f.print(spacing + "  "))
    }
}