import {Puzzle} from '../puzzle';
import {Dir} from "./Dir";

export class Day7 implements Puzzle {
    part1(input: string[]): string {
        const d = load(input)

        const total = sumDirectories(d, 100000)
        return `${total}`
    }

    part2(input: string[]): string {
        const d = load(input)
        d.print()
        const diskSize = 70000000
        const freeSpace = diskSize - d.size
        console.log('Free space: ', freeSpace)

        const required = 30000000
        const toDelete = required - freeSpace
        console.log('Need to delete: ', toDelete)
        const deleted = findSmallest(d, toDelete)
        return `${deleted}`
    }
}

export function findSmallest(dir: Dir, minSize = 0): number {
    if (dir.size < minSize) {
        throw new Error('impossible')
    }

    let smallest = dir.size

    dir.directories.forEach(d => {
        if (d.size > minSize) {
            const min = findSmallest(d, minSize)
            if (min < smallest) {
                smallest = min
            }
        }
    })

    return smallest
}

export function sumDirectories(dir: Dir, limit = 0): number {
    let total = 0

    if (dir.size <= limit) {
        total += dir.size
    }

    dir.directories.forEach(d => {
        total += sumDirectories(d, limit)
    })

    return total
}

export function load(lines: string[]): Dir {
    const topDir = new Dir("", undefined)
    let currentDir: Dir | undefined

    lines.forEach(l => {
        const tokens = l.split(" ")
        if (tokens[0] === "$") {
            const cmd = tokens[1]

            switch (cmd) {
                case "cd":
                    const name = tokens[2]
                    if (!currentDir) {
                        currentDir = topDir
                        topDir.name = name
                        return
                    }

                    if (name === "..") {
                        currentDir = currentDir.parent
                        return
                    }

                    currentDir = currentDir.directories.find(d => d.name === name)

                    return
                case "ls":
                    return
                default:
                    throw new Error(`Unrecognized command: '${cmd}'`)
            }
        } else if (tokens[0] === "dir" && currentDir) {
            currentDir.addDir(tokens[1])
        } else if (currentDir) {
            currentDir.addFile(tokens[1], parseInt(tokens[0], 10))
        }
    })

    return topDir
}
