import path from "path";
import fs from "fs";
import {Day7} from "./puzzle";

const p = path.join(__dirname, 'example.txt')
const data = fs.readFileSync(p, 'utf-8')
const lines = data.split("\n")

describe('part 1', () => {
    it('should load', () => {
        const puzzle = new Day7()
        puzzle.part1(lines)
    })
})
