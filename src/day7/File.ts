export class File {
    public name: string
    public size = 0

    constructor(name: string, size: number) {
        this.name = name
        this.size = size
    }

    // tslint:disable:no-console
    public print(spacing = "") {
        console.log(`${spacing}- ${this.name} (${this.size})`)
    }
}