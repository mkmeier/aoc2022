import {Puzzle} from '../puzzle';
import {Direction, Rope} from "./rope";

export class Day9 implements Puzzle {
    part1(input: string[]): string {
        const r = new Rope()

        input.forEach(l => {
            const tokens = l.split(' ')
            const d = parseDir(tokens[0])
            const steps = parseInt(tokens[1], 10)

            r.move(d, steps)
        })

        const uniqueMoves = new Set<string>()
        r.tailMoves.forEach(m => {
            uniqueMoves.add(m)
        })


        const result = uniqueMoves.size
        return `${result}`
    }

    part2(input: string[]): string {
        const result = ''
        return `${result}`
    }
}

function parseDir(i: string): Direction {
    switch (i) {
        case 'U':
            return Direction.UP
        case 'D':
            return Direction.DOWN
        case 'R':
            return Direction.RIGHT
        case 'L':
            return Direction.LEFT
    }

    throw new Error('Invalid Direction: ' + i)
}
