export class Position {
    public X = 0
    public Y = 0

    public String(): string {
        return `${this.X},${this.Y}`
    }
}