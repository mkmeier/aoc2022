import {Position} from "./position";


export enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}
export class Rope {

    public headPos = new Position()
    public tailPos = new Position()

    public headMoves: string[] = ['0,0']
    public tailMoves: string[] = ['0,0']

    public move(d: Direction, places = 1) {

        for (let i = 0; i < places; i++) {
            switch (d) {
                case Direction.UP:
                    this.moveUp()
                    break
                case Direction.DOWN:
                    this.moveDown()
                    break
                case Direction.LEFT:
                    this.moveLeft()
                    break
                case Direction.RIGHT:
                    this.moveRight()
                    break
                default:
                    throw new Error('invalid direction')
            }

            this.headMoves.push(this.headPos.String())
            this.moveTail()
        }
    }

    private moveUp() {
        this.headPos.Y++
    }

    private moveDown() {
        this.headPos.Y--
    }

    private moveRight() {
        this.headPos.X++
    }

    private moveLeft() {
        this.headPos.X--
    }

    private moveTail() {
        const xDis = this.tailPos.X - this.headPos.X
        const yDis = this.tailPos.Y - this.headPos.Y

        if (Math.abs(xDis) <= 1 && Math.abs(yDis) <= 1) {
            // In range. No need to move.
            return
        }

        if (yDis === 0 && Math.abs(xDis) > 1) {
            if (xDis > 0) {
                this.tailPos.X--
            } else {
                this.tailPos.X++
            }
        }

        if (xDis === 0 && Math.abs(yDis) > 1) {
            if (yDis > 0) {
                this.tailPos.Y--
            } else {
                this.tailPos.Y++
            }
        }

        if (xDis > 0 && yDis > 0) {
            this.tailPos.X--
            this.tailPos.Y--
        }

        if (xDis > 0 && yDis < 0) {
            this.tailPos.X--
            this.tailPos.Y++
        }

        if (xDis < 0 && yDis > 0) {
            this.tailPos.X++
            this.tailPos.Y--
        }

        if (xDis < 0 && yDis < 0) {
            this.tailPos.X++
            this.tailPos.Y++
        }

        this.tailMoves.push(this.tailPos.String())
    }
}