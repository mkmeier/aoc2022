import path from "path";
import fs from "fs";
import {Day9} from "./puzzle";
import {Direction, Rope} from "./rope";

const p = path.join(__dirname, 'example.txt')
const data = fs.readFileSync(p, 'utf-8')
const lines = data.split("\n")

describe('Day 9', () => {
    describe('puzzle', () => {
        it('should solve part 1', () => {
            const pzl = new Day9()
            const answer = pzl.part1(lines)

            expect(answer).toEqual('13')
        })
    })
})

describe('Rope', () => {
    it('should update position based on movements', () => {
        const r = new Rope()

        r.move(Direction.UP)
        expect(r.headPos.X).toEqual(0)
        expect(r.headPos.Y).toEqual(1)

        r.move(Direction.UP)
        expect(r.headPos.X).toEqual(0)
        expect(r.headPos.Y).toEqual(2)

        r.move(Direction.RIGHT)
        expect(r.headPos.X).toEqual(1)
        expect(r.headPos.Y).toEqual(2)

        r.move(Direction.DOWN)
        expect(r.headPos.X).toEqual(1)
        expect(r.headPos.Y).toEqual(1)

        r.move(Direction.DOWN)
        expect(r.headPos.X).toEqual(1)
        expect(r.headPos.Y).toEqual(0)

        r.move(Direction.DOWN)
        expect(r.headPos.X).toEqual(1)
        expect(r.headPos.Y).toEqual(-1)

        r.move(Direction.LEFT)
        expect(r.headPos.X).toEqual(0)
        expect(r.headPos.Y).toEqual(-1)

        r.move(Direction.LEFT)
        expect(r.headPos.X).toEqual(-1)
        expect(r.headPos.Y).toEqual(-1)
    })

    it('tail should follow to the right', () => {
        const r = new Rope()
        r.move(Direction.RIGHT, 2)
        expect(r.tailPos.X).toEqual(1)
    })

    it('tail should follow to the left', () => {
        const r = new Rope()
        r.move(Direction.LEFT, 2)
        expect(r.tailPos.X).toEqual(-1)
    })

    it('tail should follow up', () => {
        const r = new Rope()
        r.move(Direction.UP, 2)
        expect(r.tailPos.Y).toEqual(1)
    })

    it('tail should follow down', () => {
        const r = new Rope()
        r.move(Direction.DOWN, 2)
        expect(r.tailPos.Y).toEqual(-1)
    })

    it('tail should follow up and right', () => {
        const r1 = new Rope()
        r1.move(Direction.UP)
        r1.move(Direction.RIGHT)
        r1.move(Direction.UP)

        expect(r1.tailPos.X).toEqual(1)
        expect(r1.tailPos.Y).toEqual(1)

        const r2 = new Rope()
        r2.move(Direction.UP)
        r2.move(Direction.RIGHT)
        r2.move(Direction.RIGHT)

        expect(r2.tailPos.X).toEqual(1)
        expect(r2.tailPos.Y).toEqual(1)
    })

    it('tail should follow down and left', () => {
        const r1 = new Rope()
        r1.move(Direction.DOWN)
        r1.move(Direction.LEFT)
        r1.move(Direction.DOWN)

        expect(r1.tailPos.X).toEqual(-1)
        expect(r1.tailPos.Y).toEqual(-1)

        const r2 = new Rope()
        r2.move(Direction.DOWN)
        r2.move(Direction.LEFT)
        r2.move(Direction.LEFT)

        expect(r2.tailPos.X).toEqual(-1)
        expect(r2.tailPos.Y).toEqual(-1)
    })

    it('tail should follow up and left', () => {
        const r1 = new Rope()
        r1.move(Direction.UP)
        r1.move(Direction.LEFT)
        r1.move(Direction.UP)

        expect(r1.tailPos.X).toEqual(-1)
        expect(r1.tailPos.Y).toEqual(1)

        const r2 = new Rope()
        r2.move(Direction.UP)
        r2.move(Direction.LEFT)
        r2.move(Direction.LEFT)

        expect(r2.tailPos.X).toEqual(-1)
        expect(r2.tailPos.Y).toEqual(1)
    })

    it('tail should follow down and right', () => {
        const r1 = new Rope()
        r1.move(Direction.DOWN)
        r1.move(Direction.RIGHT)
        r1.move(Direction.DOWN)

        expect(r1.tailPos.X).toEqual(1)
        expect(r1.tailPos.Y).toEqual(-1)

        const r2 = new Rope()
        r2.move(Direction.DOWN)
        r2.move(Direction.RIGHT)
        r2.move(Direction.RIGHT)

        expect(r2.tailPos.X).toEqual(1)
        expect(r2.tailPos.Y).toEqual(-1)
    })
})
