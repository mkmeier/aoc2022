import {Puzzle} from '../puzzle';

export class Day6 implements Puzzle {
    part1(input: string[]): string {
        const start = locateStart(input[0])
        return `${start}`
    }

    part2(input: string[]): string {
        const start = locateMessage(input[0])
        return `${start}`
    }
}

export function locateStart(input: string): number {
    for (let i = 4; i < input.length; i++) {
        const s = new Set<string>()
        for (let j = 0; j < 5; j++) {
            s.add(input.charAt(i-j))
        }

        if (s.size >= 5) {
            return i
        }
    }

    return 0
}

export function locateMessage(input: string): number {
    for (let i = 13; i < input.length; i++) {
        const s = new Set<string>()
        for (let j = 0; j < 14; j++) {
            s.add(input.charAt(i-j))
        }

        if (s.size >= 14) {
            return i + 1
        }
    }

    return 0
}
