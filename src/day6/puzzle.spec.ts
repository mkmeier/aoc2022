import {locateMessage, locateStart} from './puzzle';

describe('locateStart', () => {
    it('example 1', () => {
        expect(locateStart('bvwbjplbgvbhsrlpgdmjqwftvncz')).toEqual(5)
    })
    it('example 2', () => {
        expect(locateStart('nppdvjthqldpwncqszvftbrmjlhg')).toEqual(6)
    })
    it('example 3', () => {
        expect(locateStart('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg')).toEqual(10)
    })
    it('example 4', () => {
        expect(locateStart('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw')).toEqual(11)
    })
})

describe('locateMessage', () => {
    it('example 1', () => {
        expect(locateMessage('mjqjpqmgbljsphdztnvjfqwrcgsmlb')).toEqual(19)
    })
    it('example 2', () => {
        expect(locateMessage('bvwbjplbgvbhsrlpgdmjqwftvncz')).toEqual(23)
    })
    it('example 3', () => {
        expect(locateMessage('nppdvjthqldpwncqszvftbrmjlhg')).toEqual(23)
    })
    it('example 4', () => {
        expect(locateMessage('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg')).toEqual(29)
    })
    it('example 4', () => {
        expect(locateMessage('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw')).toEqual(26)
    })
})
