import {Command} from "commander";
import {puzzles} from "./registry";
import * as path from "path";
import * as fs from "fs";

const program = new Command()

program.requiredOption('-d, --day <day>',  'Puzzle Day')
program.option('-f, --file <day>', 'Name of the input file', 'input.txt')
program.option('-p, --part <part>', 'Which puzzle part to run', '1')

program.parse(process.argv)

const opts = program.opts()
console.log(opts)
const day = opts.day
const part = opts.part
const file = opts.file

const puzzle = puzzles.get(day)
if (!puzzle) {
    console.error(`No puzzle found for day ${day}`)
    process.exit(1)
}

const p = path.join(__dirname, `../day${day}/${file}`)
console.log("Input File: ", p)

let data: string
try {
    data = fs.readFileSync(p, 'utf-8')
} catch (e) {
    console.error('Failed to read file: ' + e)
    process.exit(1)
}

const lines = data.split("\n")

if (puzzle) {
    const start = new Date().getMilliseconds()
    const answer = part === '2' ? puzzle.part2(lines) : puzzle.part1(lines)

    console.log('Answer: ', answer)
    console.log(`Finished in: ${new Date().getMilliseconds() - start} ms`)
} else {
    console.log('Puzzle not found')
}
