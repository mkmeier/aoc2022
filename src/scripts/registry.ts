import {Puzzle} from "../puzzle";
import {Day1} from "../day1/run";
import {Day2} from "../day2/puzzle";
import {Day3} from "../day3/puzzle";
import {Day4} from "../day4/puzzle";
import {Day5} from '../day5/puzzle';
import {Day6} from '../day6/puzzle';
import {Day7} from "../day7/puzzle";
import {Day8} from "../day8/puzzle";
import {Day9} from "../day9/puzzle";

export const puzzles: Map<string, Puzzle> = new Map()

// TODO: Dynamically load the puzzles
puzzles.set('1', new Day1())
puzzles.set('2', new Day2())
puzzles.set('3', new Day3())
puzzles.set('4', new Day4())
puzzles.set('5', new Day5())
puzzles.set('6', new Day6())
puzzles.set('7', new Day7())
puzzles.set('8', new Day8())
puzzles.set('9', new Day9())
