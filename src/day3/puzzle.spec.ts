import {Day3, findCommon, findCommonTriple, score, split} from "./puzzle";

describe('split', () => {
    it('test 1', () => {
        expect(split("abcdef")).toEqual(["abc", "def"])
        expect(split("abcdefgh")).toEqual(["abcd", "efgh"])
    })
})

describe('findMatch', () => {
    it('test 1', () => {
        expect(findCommon("abcdefgh", "ijklmnope")).toEqual("e")
    })
})

describe("score", () => {
    it('test 1', () => {
        expect(score("a")).toEqual(1)
        expect(score("z")).toEqual(26)
        expect(score("A")).toEqual(27)
        expect(score("Z")).toEqual(52)
    })
})

describe("findCommonTriple", () => {
    it('test 1', () => {
        expect(findCommonTriple(
            "vJrwpWtwJgWrhcsFMMfFFhFp",
            "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
            "PmmdzqPrVvPwwTWBwg"))
            .toEqual("r")
    })

    it('test 2', () => {
        expect(findCommonTriple(
            "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
            "ttgJtRGJQctTZtZT",
            "CrZsJsPPZsGzwwsLwLmpwMDw"))
            .toEqual("Z")
    })
})

describe('part 2', () => {
    it('should total 70', () => {
        const input = [
            'vJrwpWtwJgWrhcsFMMfFFhFp',
            'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
            'PmmdzqPrVvPwwTWBwg',
            'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
            'ttgJtRGJQctTZtZT',
            'CrZsJsPPZsGzwwsLwLmpwMDw',
        ]

        const p = new Day3()
        const result = p.part2(input)
        expect(result).toEqual("70")
    })
})

