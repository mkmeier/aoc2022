import {Puzzle} from "../puzzle";

export class Day3 implements Puzzle {
    part1(input: string[]): string {
        let total = 0
        for (const line of input) {
            const compartments  = split(line)
            total += score(findCommon(compartments[0], compartments[1]))
        }
        return `${total}`
    }

    part2(input: string[]): string {
        let group: string[] = []
        let total = 0
        for (const line of input) {
            group.push(line)

            if (group.length > 2) {
                const match = findCommonTriple(group[0], group[1], group[2])
                const value = score(match)
                total += value
                group = []
            }
        }

        return `${total}`
    }
}

export function split(s: string): string[] {
    const pivot = s.length/2
    return [s.slice(0, pivot), s.slice(pivot)]
}

export function findCommon(s1: string, s2: string): string {
    const set = new Set<string>()
    for (let i = 0; i < s2.length; i++) {
        set.add(s2.charAt(i))
    }
    for (let i = 0; i < s1.length; i++) {
        const c = s1.charAt(i)
        if (set.has(c)) {
            return c
        }
    }

    return ""
}

export function findCommonTriple(s1: string, s2: string, s3: string): string {
    const set1 = buildSet(s1)
    const set2 = buildSet(s2)

    for (let i = 0; i < s3.length; i++) {
        const c = s3.charAt(i)
            if (set1.has(c) && set2.has(c)) {
                return c
            }
    }

    return ""
}

export function buildSet(s: string): Set<string> {
    const set = new Set<string>()
    for (let i = 0; i < s.length; i++) {
        set.add(s.charAt(i))
    }

    return set
}

export function score(c: string): number {
    if (!c) {
        return 0
    }
    const code = c.charCodeAt(0)
    const score = code - 96
    if (score < 1) {
        return code - 38
    }

    return score
}