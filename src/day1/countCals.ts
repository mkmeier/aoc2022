export function findMostCals(input: string[]): string {

    let highValue = 0
    let currentValue = 0

    for (const line of input) {
        if (!line) {
            highValue = currentValue > highValue ? currentValue : highValue
            if (currentValue > highValue) {
                highValue = currentValue
            }
            currentValue = 0
            continue
        }

        currentValue += parseInt(line)
    }
    highValue = currentValue > highValue ? currentValue : highValue

    return highValue.toString(10)
}

export function topThree(input: string[]): string {
    const cals: number[] = []

    let currentValue = 0
    for (const line of input) {
        if (!line) {
            cals.push(currentValue)
            currentValue = 0
            continue
        }

        currentValue += parseInt(line)
    }
    // don't forget the last one.
    cals.push(currentValue)

    cals.sort((n1, n2) => n2 - n1)

    const limit = 3
    let i = 0
    let total = 0
    for(const c of cals) {
        if (i++ >= limit) {
            break
        }

        total += c
    }

    return total.toString(10)
}

