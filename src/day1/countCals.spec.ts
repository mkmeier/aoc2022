import {findMostCals, topThree} from "./countCals";

const example = [
    '1000',
    '2000',
    '3000',
    '',
    '4000',
    '',
    '5000',
    '6000',
    '',
    '7000',
    '8000',
    '9000',
    '',
    '10000',
]

describe('findMostCals', () => {
    it('should return 24000 for test input', () => {
        expect(findMostCals(example)).toEqual('24000')
    })
})

describe('topThree', () => {
    it('should return 45000', () => {
        expect(topThree(example)).toEqual('45000')
    })
})