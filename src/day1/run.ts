import {Puzzle} from "../puzzle";
import {findMostCals, topThree} from "./countCals";

export class Day1 implements Puzzle {
    part1(input: string[]): string {
        return findMostCals(input)
    }

    part2(input: string[]): string {
        return topThree(input);
    }

}
