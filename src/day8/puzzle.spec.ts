import path from "path";
import fs from "fs";
import {
    computeScenicScore,
    Day8,
    getTree,
    isVisible,
    isVisibleFromE,
    isVisibleFromN,
    isVisibleFromS,
    isVisibleFromW,
    parseForest
} from "./puzzle";

const p = path.join(__dirname, 'example.txt')
const data = fs.readFileSync(p, 'utf-8')
const lines = data.split("\n")
const f = parseForest(lines)

describe('day 8', () => {
    it('should solve', () => {
        const pzl = new Day8()
        const result = pzl.part1(lines)

        expect(result).toEqual('21')
    })
})

describe('scenicScore', () => {
    it('should be 4 for 2, 1', () => {
        expect(computeScenicScore(f, 2, 1)).toEqual(4)
    })

    it('should be 4 for 2, 4', () => {
        expect(computeScenicScore(f, 2, 3)).toEqual(8)
    })
})

describe('isVisibleFromN', () => {

    it('should be true for top row', () => {
        expect(isVisibleFromN(f, 3, 0)).toEqual(true)
    })

    it('should be true if tallest', () => {
        expect(isVisibleFromN(f, 1, 1)).toEqual(true)
        expect(isVisibleFromN(f, 2, 1)).toEqual(true)
        expect(isVisibleFromN(f, 4, 3)).toEqual(true)
    })

    it('should be false if one tree is taller', () => {
        expect(isVisibleFromN(f, 2, 2)).toEqual(false)
        expect(isVisibleFromN(f, 3, 1)).toEqual(false)
        expect(isVisibleFromN(f, 1, 2)).toEqual(false)
    })
})

describe('isVisibleFromS', () => {
    it('should be true for bottom row', () => {
        expect(isVisibleFromS(f, 0, 4)).toEqual(true)
        expect(isVisibleFromS(f, 1, 4)).toEqual(true)
        expect(isVisibleFromS(f, 2, 4)).toEqual(true)
    })

    it('should be true if tallest', () => {
        expect(isVisibleFromS(f, 0, 2)).toEqual(true)
        expect(isVisibleFromS(f, 2, 3)).toEqual(true)
        expect(isVisibleFromS(f, 4, 3)).toEqual(true)
    })

    it('should be false if one tree is taller', () => {
        expect(isVisibleFromS(f, 1, 3)).toEqual(false)
        expect(isVisibleFromS(f, 3, 3)).toEqual(false)
    })
})

describe('isVisibleFromE', () => {
    it('should be true for right column', () => {
        expect(isVisibleFromE(f, 4, 0)).toEqual(true)
        expect(isVisibleFromE(f, 4, 1)).toEqual(true)
        expect(isVisibleFromE(f, 4, 2)).toEqual(true)
    })

    it('should be true if tallest', () => {
        expect(isVisibleFromE(f, 3, 0)).toEqual(true)
        expect(isVisibleFromE(f, 4, 3)).toEqual(true)
        expect(isVisibleFromE(f, 2, 1)).toEqual(true)
        expect(isVisibleFromE(f, 0, 2)).toEqual(true)
    })

    it('should be false if one tree is taller', () => {
        expect(isVisibleFromE(f, 2, 0)).toEqual(false)
        expect(isVisibleFromE(f, 0, 3)).toEqual(false)
    })
})

describe('isVisibleFromW', () => {
    it('should be true for left column', () => {
        expect(isVisibleFromW(f, 0, 0)).toEqual(true)
        expect(isVisibleFromW(f, 0, 1)).toEqual(true)
        expect(isVisibleFromW(f, 0, 2)).toEqual(true)
    })

    it('should be true if tallest', () => {
        expect(isVisibleFromW(f, 1, 1)).toEqual(true)
        expect(isVisibleFromW(f, 2, 3)).toEqual(true)
    })

    it('should be false if one tree is taller', () => {
        expect(isVisibleFromW(f, 1, 0)).toEqual(false)
        expect(isVisibleFromW(f, 2, 2)).toEqual(false)
    })
})

describe('isVisible', () => {
    it('should be visible for 1,1', () => {
        expect(isVisible(f, 1, 1)).toEqual(true)
    })

    it('should be visible for 2,1', () => {
        expect(isVisible(f, 2, 1)).toEqual(true)
    })

    it('should not be visible for 3,1', () => {
        expect(isVisible(f, 3, 1)).toEqual(false)
    })

    it('should be visible for 1,2', () => {
        expect(isVisible(f, 1, 2)).toEqual(true)
    })

    it('should not be visible for 2,2', () => {
        expect(isVisibleFromN(f, 2, 2)).toEqual(false)
        expect(isVisibleFromS(f, 2, 2)).toEqual(false)
        expect(isVisibleFromE(f, 2, 2)).toEqual(false)
        expect(isVisibleFromW(f, 2, 2)).toEqual(false)

        expect(isVisible(f, 2, 2)).toEqual(false)
    })

    it('should be visible for 3,2', () => {
        expect(isVisible(f, 3, 2)).toEqual(true)
    })

    it('should be visible for 2,3', () => {
        expect(isVisible(f, 2, 3)).toEqual(true)
    })
})

describe('getTree', () => {
    it('should return tree', () => {
        expect(getTree(f, 0, 0)).toEqual(3)
        expect(getTree(f, 1, 1)).toEqual(5)
        expect(getTree(f, 2, 2)).toEqual(3)
        expect(getTree(f, 3, 3)).toEqual(4)

        expect(getTree(f, 0, 1)).toEqual(2)
        expect(getTree(f, 0, 2)).toEqual(6)
        expect(getTree(f, 0, 3)).toEqual(3)

        expect(getTree(f, 2, 1)).toEqual(5)
        expect(getTree(f, 3, 1)).toEqual(1)
        expect(getTree(f, 4, 1)).toEqual(2)

        expect(getTree(f, 4, 3)).toEqual(9)
    })
})
