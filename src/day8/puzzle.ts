import {Puzzle} from '../puzzle';

type Forest = number[][]
export class Day8 implements Puzzle {
    part1(input: string[]): string {
        const f = parseForest(input)

        const maxX = f[0].length - 1
        const maxY = f.length - 1

        let count = 0

        for (let x = 0; x <= maxX; x++) {
            for (let y = 0; y <= maxY; y++) {
                if (isVisible(f, x, y)) {
                    count++
                }
            }
        }

        return `${count}`
    }

    part2(input: string[]): string {
        const f = parseForest(input)

        const maxX = f[0].length - 1
        const maxY = f.length - 1

        let maxScore = 0

        for (let x = 0; x <= maxX; x++) {
            for (let y = 0; y <= maxY; y++) {
                const score = computeScenicScore(f, x, y)
                if (score > maxScore) {
                    maxScore = score
                }
            }
        }

        return `${maxScore}`
    }
}

export function parseForest(lines: string[]): Forest {
    const forest: number[][] = []
    lines.forEach(l => {
        const row: number[] = []
        for (let i = 0; i < l.length; i++) {
            const c = l.charAt(i)
            row.push(parseInt(c, 10))
        }
        forest.push(row)
    })

    return forest
}

export function isVisible(f: Forest, x: number, y: number) {
    return isVisibleFromN(f, x, y) || isVisibleFromS(f, x, y) || isVisibleFromE(f, x, y) || isVisibleFromW(f, x, y)
}

export function isVisibleFromN(f: Forest, x: number, y: number): boolean {
    if (y === 0) {
        return true
    }

    const height = getTree(f, x, y)

    for (let i = y - 1; i >= 0; i--) {
        const tree = getTree(f, x, i)
        if (tree >= height) {
            return false
        }
    }

    return true
}

export function isVisibleFromS(f: Forest, x: number, y: number): boolean {
    const max = f.length - 1
    if (y === max) {
        return true
    }

    const height = getTree(f, x, y)

    for (let i = y + 1; i < f.length; i++) {
        const tree = getTree(f, x, i)
        if (tree >= height) {
            return false
        }
    }

    return true
}

export function isVisibleFromE(f: Forest, x: number, y: number): boolean {
    const max = f[0].length - 1
    if (x === max) {
        return true
    }

    const height = getTree(f, x, y)

    for (let i = x + 1; i <= max; i++) {
        const tree = getTree(f, i, y)
        if (tree >= height) {
            return false
        }
    }

    return true
}

export function isVisibleFromW(f: Forest, x: number, y: number): boolean {
    const max = f[0].length - 1
    if (x === 0) {
        return true
    }

    const height = getTree(f, x, y)

    for (let i = x - 1; i >= 0; i--) {
        const tree = getTree(f, i, y)
        if (tree >= height) {
            return false
        }
    }

    return true
}

export function getTree(f: Forest, x: number, y: number) {
    return f[y][x]
}

export function computeScenicScore(f: Forest, x: number, y: number): number {
    const n = countVisibleFromN(f, x, y)
    const s = countVisibleFromS(f, x, y)
    const e = countVisibleFromE(f, x, y)
    const w = countVisibleFromW(f, x, y)

    return n * s * e * w
}

export function countVisibleFromN(f: Forest, x: number, y: number): number {
    let count = 0
    if (y === 0) {
        return count
    }

    const height = getTree(f, x, y)

    for (let i = y - 1; i >= 0; i--) {
        count ++

        const tree = getTree(f, x, i)
        if (tree >= height) {
            return count
        }
    }

    return count
}

export function countVisibleFromS(f: Forest, x: number, y: number): number {
    let count = 0
    const max = f.length - 1
    if (y === max) {
        return count
    }

    const height = getTree(f, x, y)

    for (let i = y + 1; i < f.length; i++) {
        count ++

        const tree = getTree(f, x, i)
        if (tree >= height) {
            return count
        }
    }

    return count
}

export function countVisibleFromE(f: Forest, x: number, y: number): number {
    let count = 0
    const max = f[0].length - 1
    if (x === max) {
        return count
    }

    const height = getTree(f, x, y)

    for (let i = x + 1; i <= max; i++) {
        count ++

        const tree = getTree(f, i, y)
        if (tree >= height) {
            return count
        }
    }

    return count
}

export function countVisibleFromW(f: Forest, x: number, y: number): number {
    let count = 0
    if (x === 0) {
        return count
    }

    const height = getTree(f, x, y)

    for (let i = x - 1; i >= 0; i--) {
        count ++

        const tree = getTree(f, i, y)
        if (tree >= height) {
            return count
        }
    }

    return count
}

