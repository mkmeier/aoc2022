import {isIn, newDef, overlap} from "./puzzle";

describe('isIn', () => {
    it('should work', () => {
        expect(isIn(newDef('2-4'), newDef('6-8'))).toEqual(false)
        expect(isIn(newDef('2-3'), newDef('4-5'))).toEqual(false)
        expect(isIn(newDef('5-7'), newDef('7-9'))).toEqual(false)
        expect(isIn(newDef('3-7'), newDef('2-8'))).toEqual(true)
        expect(isIn(newDef('6-6'), newDef('4-6'))).toEqual(true)
        expect(isIn(newDef('2-6'), newDef('4-8'))).toEqual(false)
    })
})

describe('overlap', () => {
    it('should work', () => {
        expect(overlap(newDef('2-4'), newDef('6-8'))).toEqual(false)
        expect(overlap(newDef('2-3'), newDef('4-5'))).toEqual(false)
        expect(overlap(newDef('5-7'), newDef('7-9'))).toEqual(true)
        expect(overlap(newDef('2-8'), newDef('3-7'))).toEqual(true)
        expect(overlap(newDef('6-6'), newDef('4-6'))).toEqual(true)
        expect(overlap(newDef('2-6'), newDef('4-8'))).toEqual(true)
    })
})

describe('newDef', () => {
    it('should create', () => {
        expect(newDef('2-3')).toEqual({start: 2, end: 3})
        expect(newDef('4-8')).toEqual({start: 4, end: 8})
    })
})
