import {Puzzle} from "../puzzle";

export class Day4 implements Puzzle {
    part1(input: string[]): string {
        let count = 0
        for (const line of input) {
            const inputs = line.split(',')
            const d1 = newDef(inputs[0])
            const d2 = newDef(inputs[1])

            if (isIn(d1, d2) || isIn(d2, d1)) {
                count ++
            }
        }

        return `${count}`
    }

    part2(input: string[]): string {
        let count = 0
        for (const line of input) {
            const inputs = line.split(',')
            const d1 = newDef(inputs[0])
            const d2 = newDef(inputs[1])

            if (overlap(d1, d2)) {
                count ++
            }
        }

        return `${count}`
    }
}

export interface Def {
    start: number,
    end: number,
}

export function newDef(s: string): Def {
    const nums = s.split('-')
    return {
        start: parseInt(nums[0], 10),
        end: parseInt(nums[1], 10),
    }
}

export function isIn(d1: Def, d2: Def): boolean {
    return d1.start >= d2.start && d1.end <= d2.end
}

export function overlap(d1: Def, d2: Def): boolean {
    return !(d1.end < d2.start || d1.start > d2.end);
}
