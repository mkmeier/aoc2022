import {Hand, Outcome, play} from "./puzzle";

describe('play', () => {
    it('should follow rock paper scissor rules', () => {
        expect(play(Hand.ROCK, Hand.PAPER)).toEqual(Outcome.LOSE)
        expect(play(Hand.ROCK, Hand.SCISSORS)).toEqual(Outcome.WIN)
        expect(play(Hand.ROCK, Hand.ROCK)).toEqual(Outcome.DRAW)
        expect(play(Hand.PAPER, Hand.PAPER)).toEqual(Outcome.DRAW)
        expect(play(Hand.PAPER, Hand.SCISSORS)).toEqual(Outcome.LOSE)
        expect(play(Hand.PAPER, Hand.ROCK)).toEqual(Outcome.WIN)
        expect(play(Hand.SCISSORS, Hand.PAPER)).toEqual(Outcome.WIN)
        expect(play(Hand.SCISSORS, Hand.SCISSORS)).toEqual(Outcome.DRAW)
        expect(play(Hand.SCISSORS, Hand.ROCK)).toEqual(Outcome.LOSE)
    })
})