import {Puzzle} from "../puzzle";

export enum Hand {
    ROCK = "ROCK",
    PAPER = "PAPER",
    SCISSORS = "SCISSORS",
}

export enum Outcome {
    WIN = "WIN",
    LOSE = "LOSE",
    DRAW = "DRAW",
}

export class Day2 implements Puzzle {
    part1(input: string[]): string {
        let total = 0
        for(const line of input) {
            const plays = line.split(" ")
            const opp = parse(plays[0])
            const mine = parse(plays[1])
            const result = play(mine, opp)

            const score = scoreHand(mine) + scoreOutcome(result)
            total += score
        }
        return `${total}`;
    }

    part2(input: string[]): string {
        let total = 0
        for(const line of input) {
            const plays = line.split(" ")
            const opp = parse(plays[0])
            const desiredResult = parseOutcome(plays[1])
            const mine = getHand(opp, desiredResult)
            const result = play(mine, opp)

            console.log(`${opp}:${mine} => ${result} (Desired: ${desiredResult})`)
            const score = scoreHand(mine) + scoreOutcome(result)
            console.log(score)
            total += score
        }
        return `${total}`;
    }
}

export function scoreHand(h: Hand): number {
    switch(h) {
        case Hand.ROCK:
            return 1
        case Hand.PAPER:
            return 2
        case Hand.SCISSORS:
            return 3
    }

    return 0;
}

export function scoreOutcome(o: Outcome): number {
    switch (o) {
        case Outcome.WIN:
            return 6
        case Outcome.LOSE:
            return 0
        case Outcome.DRAW:
            return 3

    }
}

export function getHand(opp: Hand, desiredOutcome: Outcome): Hand {
    if (desiredOutcome == Outcome.DRAW) {
        return opp
    }

    switch (opp) {
        case Hand.ROCK:
            switch (desiredOutcome) {
                case Outcome.WIN:
                    return Hand.PAPER
                case Outcome.LOSE:
                    return Hand.SCISSORS
            }
            break
        case Hand.PAPER:
            switch (desiredOutcome) {
                case Outcome.WIN:
                    return Hand.SCISSORS
                case Outcome.LOSE:
                    return Hand.ROCK
            }
            break
        case Hand.SCISSORS:
            switch (desiredOutcome) {
                case Outcome.WIN:
                    return Hand.ROCK
                case Outcome.LOSE:
                    return Hand.PAPER
            }
            break
    }
}

export function play(mine: Hand, opp: Hand): Outcome {
    switch (mine) {
        case Hand.ROCK:
            switch (opp) {
                case Hand.ROCK:
                    return Outcome.DRAW
                case Hand.PAPER:
                   return Outcome.LOSE
                case Hand.SCISSORS:
                    return Outcome.WIN;
            }
            break
        case Hand.PAPER:
            switch (opp) {
                case Hand.ROCK:
                    return Outcome.WIN
                case Hand.PAPER:
                    return Outcome.DRAW
                case Hand.SCISSORS:
                    return Outcome.LOSE;
            }
            break
        case Hand.SCISSORS:
            switch (opp) {
                case Hand.ROCK:
                    return Outcome.LOSE
                case Hand.PAPER:
                    return Outcome.WIN
                case Hand.SCISSORS:
                    return Outcome.DRAW;
            }
    }
}

export function parseOutcome(input: string): Outcome {
    switch (input) {
        case 'X':
            return Outcome.LOSE
        case 'Y':
            return Outcome.DRAW
        case 'Z':
            return Outcome.WIN
    }
    throw new Error('Invalid outcome: ' + input)
}

export function parse(input: string): Hand {
    switch (input) {
        case 'A':
        case 'X':
            return Hand.ROCK
        case 'B':
        case 'Y':
            return Hand.PAPER
        case 'C':
        case 'Z':
            return Hand.SCISSORS
    }

    throw new Error('Invalid hand: ' + input)
}
